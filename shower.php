<?php 

if (!file_exists("img/about.csv")){
	include_once("creator.php");
}

// Открываем документ для чтения
$file = fopen("img/about.csv", "r");
$files = array();

// Записываем в двухмерный массив (имена файлов в качестве ключе) из документа необходимые значения 
while(!feof($file)){
	$fileinfo = fgetcsv($file);
	$fileabout = array();
	$fileabout["Название"] = $fileinfo[0];
	$fileabout["Размер"] = $fileinfo[8]/1000;
	$fileabout["Время последнего доступа"] = date('Y-m-d h:i', (int)$fileinfo[9]); 
	$fileabout["Время последней модификации"] = date('Y-m-d h:i', (int)$fileinfo[10]); 
	$fileabout["Время последнего изменения"] = date('Y-m-d h:i', (int)$fileinfo[11]);
	$files[$fileinfo[0]] = $fileabout;	
}
fclose($file);
