<?php
   // Проверяем разрешение файла
   if(strpos(strtolower($_FILES["filename"]["name"]), ".jpg") === false)
   {
   		// Если разрешение не подходит сообщаем об этом
      $message = "Вы попытались загрузить файл в формате, отличном от jpg";
   	header("Location: index.php?message=$message");
      die;
   }
  
  // Проверяем загружен ли файл
   if(is_uploaded_file($_FILES["filename"]["tmp_name"]))
   {
      // Если файл загружен успешно, перемещаем его
      // из временной директории в конечную
      move_uploaded_file($_FILES["filename"]["tmp_name"], "img/{$_POST['filecount']}.jpg");
      include "creator.php";

      header("Location: index.php");
      die;
   } 
   else 
   {
      $message = "Ошибка загрузки файла";
      //echo "Ошибка загрузки файла";
      //header("Location: index.php?message=$message");
   }

