<?php

$filename = 1;

$about = fopen("img/about.csv", "w"); //открываем файл csv для записи


while(file_exists('img/'.$filename.'.jpg'))
{

	//получаем массив параметров файла
	$filepath = 'img/'.$filename.'.jpg';
	$fileinfo = stat($filepath);
	$fileinfo = array_splice($fileinfo, count($fileinfo)/2);
	
	// записываем в csv первой строкой название параметров
	if($filename==1)
	{
		$filekey = array_keys($fileinfo);
		array_unshift($filekey, "title"); // дополняем массив параметром 'title' 
		fputcsv($about, $filekey);			
	}
	
	// записываем параметры файла в файл csv
	array_unshift($fileinfo, $filename.".jpg"); // дополняем массив значением параметра 'title'
	fputcsv($about, $fileinfo);
	
	// создаем уменьшенную копию картинки, если она не создана
	if(!file_exists('img/'.$filename.'_small.jpg')){
		$newsize = 120;
		list($width, $height) = getimagesize($filepath);
		list($width_small, $height_small, $x, $y) = getNewSize($width, $height, $newsize);
		$file_r = imagecreatetruecolor($width_small, $height_small);
		$file_mini = imagecreatefromjpeg($filepath);
		imagecopyresampled($file_r, $file_mini, 0, 0, 0, 0, $width_small, $height_small, $width, $height);
		$file_mini = imagecreatetruecolor($newsize, $newsize);
		$color = imagecolorallocate($file_mini, 200, 200, 200);
		imagefill($file_mini, 0, 0, $color);
		imagecopy($file_mini, $file_r, $x, $y, 0, 0, $width_small, $height_small);
		imagejpeg($file_mini, "img/".$filename."_small.jpg");
	}
	$filename++;
}

fclose($about);

function getNewSize($width, $height, $a)
{
	if($width >= $height) 
	{
		$height = ceil($height/$width*$a);
		$width = $a;
		$x = 0;
		$y = ($width - $height) / 2; 
	}
	else
	{
		$width = ceil($width/$height*$a);
		$height = $a;
		$y = 0;
		$x = ($height - $width) / 2;
	}
	return array($width , $height, $x, $y);
}

function intdiv($a, $b){
    return ($a - $a % $b) / $b;
}